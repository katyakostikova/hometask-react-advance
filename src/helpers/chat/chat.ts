import { MessageModel } from '../../models/chat/Message';

const checkLastUserMessage = (
  messages: MessageModel[],
  currentUserId: string
) => {
  const messageArr = [...messages].reverse();
  const mes = messageArr.find(message => message.userId === +currentUserId);

  return mes?.id;
};

const getChatParticipants = (messages: MessageModel[]) => {
  const usersSet = new Set();
  messages.forEach(messageData => usersSet.add(messageData.userId));
  return usersSet.size;
};

export { checkLastUserMessage, getChatParticipants };
