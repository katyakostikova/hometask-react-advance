/* eslint-disable no-case-declarations */
import { DAY_OF_WEEK, MONTHS } from '../../constants/enums/date';

const checkDividerDate = (date: string) => {
  const currentDate = new Date();
  const messageDate = new Date(date);
  const currentDateString = `${currentDate.getDate()} ${currentDate.getMonth()}`;
  const yesterdayDateString = `${
    currentDate.getDate() - 1
  } ${currentDate.getMonth()}`;
  const messageDateString = `${messageDate.getDate()} ${messageDate.getMonth()}`;

  switch (messageDateString) {
    case currentDateString:
      return 'Today';
    case yesterdayDateString:
      return 'Yesterday';
    default:
      const dayString = `${DAY_OF_WEEK.get(
        messageDate.getDay()
      )}, ${messageDate.getDate()} ${MONTHS.get(messageDate.getMonth())}`;
      return dayString;
  }
};

const checkIsDifferentDate = (firstDate: string, secondDate: string) => {
  if (firstDate && secondDate) {
    const firstDateParsed = new Date(firstDate);
    const secondDateParsed = new Date(secondDate);
    if (firstDateParsed.getDate() === secondDateParsed.getDate()) return false;
    return true;
  }
  return false;
};

export { checkDividerDate, checkIsDifferentDate };
