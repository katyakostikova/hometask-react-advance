import { MessageModel } from './models/chat/Message';
import { Chat } from './services/chat/chat-service';

// export interface MessageData {
//   id: string;
//   userId: string;
//   avatar: string;
//   user: string;
//   text: string;
//   createdAt: string;
//   editedAt: string;
//   likedBy?: string[];
// }

// export type MessagesState = {
//   messages: MessageData[] | [];
// };

export type MessagesAction = {
  type: string;
  payload: {
    messages?: MessageModel[] | [];
    messageText?: string;
    messageId?: string;
    isLiked?: boolean;
  };
};

export type AsyncThunkConfig = {
  extra?: {
    service: {
      chat: Chat;
    };
  };
};
