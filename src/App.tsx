import React from 'react';
import './App.css';
import { Routes, Route, Navigate } from 'react-router-dom';
import Chat from './components/chat-page/chat/Chat';
import Login from './components/login/Login';
import UserPage from './components/user-page/user-container/UserPage';
import UserEdit from './components/user-page/user-action/UserEdit';
import NotFound from './components/not-found/NotFound';
import AppLogo from './assets/images/logo.png';

const App = () => (
  <div className="App">
    <div className="logo">
      <img className="logo-img" alt="logo" src={AppLogo} />
      <h2>Binary Studio Academy hometask chat</h2>
    </div>
    <Routes>
      <Route path="/" element={<Navigate replace to="/login" />} />
      <Route path="/login" element={<Login />} />
      <Route path="/chat" element={<Chat />} />
      <Route path="/users" element={<UserPage />} />
      <Route path="/users/edit" element={<UserEdit />} />
      <Route path="/users/edit/:id" element={<UserEdit />} />
      <Route path="*" element={<NotFound />} />
    </Routes>
  </div>
);

export default App;
