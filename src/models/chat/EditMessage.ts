export interface EditMessageModel {
  text: string;
  liked: boolean;
}
