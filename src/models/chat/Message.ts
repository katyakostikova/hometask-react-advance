export interface MessageModel {
  id: string;
  text: string;
  createdAt: string;
  updatedAt: string | null;
  liked: boolean;
  userId: number;
  avatar: string | null;
  user: string | null;
}
