export interface NewMessageModel {
  text: string;
  liked: boolean;
}
