export interface UserEditModel {
  id: string | undefined;
  name: string;
  email: string;
  password: string;
  avatar: string;
}
