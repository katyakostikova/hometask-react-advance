export interface UserModel {
  id: string;
  name: string;
  email: string;
  role: string;
  avatar: string | undefined;
  token: string | undefined;
}
