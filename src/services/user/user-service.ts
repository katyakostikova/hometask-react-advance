import { contentType } from '../../constants/http/content-type';
import { httpMethods } from '../../constants/http/http-methods';
import { LoginModel } from '../../models/user/Login';
import { UserEditModel } from '../../models/user/UserEdit';

class User {
  _apiPath: string;

  constructor(apiPath: string) {
    this._apiPath = apiPath;
  }

  login(userLoginData: LoginModel) {
    return fetch(`${this._apiPath}/Auth/login`, {
      method: httpMethods.POST,
      body: JSON.stringify(userLoginData),
      headers: {
        'Content-Type': contentType.JSON
      }
    }).then(res => res.json());
  }

  loadUsers(token: string) {
    return fetch(`${this._apiPath}/Users`, {
      method: httpMethods.GET,
      headers: {
        authorization: `Bearer ${token}`
      }
    }).then(res => res.json());
  }

  deleteUserById(id: string, token: string) {
    return fetch(`${this._apiPath}/Users/${id}`, {
      method: httpMethods.DELETE,
      body: JSON.stringify({
        id
      }),
      headers: {
        'Content-Type': contentType.JSON,
        authorization: `Bearer ${token}`
      }
    });
  }

  createUser(userData: UserEditModel, token: string) {
    return fetch(`${this._apiPath}/Users`, {
      method: httpMethods.POST,
      headers: {
        'Content-Type': contentType.JSON,
        authorization: `Bearer ${token}`
      },
      body: JSON.stringify(userData)
    }).then(res => res.json());
  }

  updateUser(id: string, userData: UserEditModel, token: string) {
    return fetch(`${this._apiPath}/Users/${id}`, {
      method: httpMethods.PUT,
      headers: {
        'Content-Type': contentType.JSON,
        authorization: `Bearer ${token}`
      },
      body: JSON.stringify(userData)
    }).then(res => res.json());
  }

  getUserById(id: string, token: string) {
    return fetch(`${this._apiPath}/Users/${id}`, {
      method: httpMethods.GET,
      headers: {
        authorization: `Bearer ${token}`
      }
    }).then(res => res.json());
  }
}

export { User };
