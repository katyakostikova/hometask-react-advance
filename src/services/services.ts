import { Chat } from './chat/chat-service';
import { API_PATH } from '../constants/http/api';
import { User } from './user/user-service';

const chatService = new Chat(API_PATH);
const userService = new User(API_PATH);

export { chatService, userService };
