import { contentType } from '../../constants/http/content-type';
import { httpMethods } from '../../constants/http/http-methods';
import { EditMessageModel } from '../../models/chat/EditMessage';
import { NewMessageModel } from '../../models/chat/NewMessage';

class Chat {
  _apiPath: string;

  constructor(apiPath: string) {
    this._apiPath = apiPath;
  }

  getAllMessages(token: string) {
    return fetch(`${this._apiPath}/Messages`, {
      method: httpMethods.GET,
      headers: {
        authorization: `Bearer ${token}`
      }
    }).then(res => res.json());
  }

  addMessage(message: NewMessageModel, token: string) {
    return fetch(`${this._apiPath}/Messages`, {
      method: httpMethods.POST,
      headers: {
        'Content-Type': contentType.JSON,
        authorization: `Bearer ${token}`
      },
      body: JSON.stringify(message)
    }).then(res => res.json());
  }

  deleteMessageById(id: string, token: string) {
    return fetch(`${this._apiPath}/Messages/${id}`, {
      method: httpMethods.DELETE,
      headers: {
        authorization: `Bearer ${token}`
      }
    });
  }

  getMessageById(id: string, token: string) {
    return fetch(`${this._apiPath}/Messages/${id}`, {
      method: httpMethods.GET,
      headers: {
        authorization: `Bearer ${token}`
      }
    }).then(res => res.json());
  }

  editMessage(id: string, message: EditMessageModel, token: string) {
    return fetch(`${this._apiPath}/Messages/${id}`, {
      method: httpMethods.PUT,
      headers: {
        'Content-Type': contentType.JSON,
        authorization: `Bearer ${token}`
      },
      body: JSON.stringify(message)
    }).then(res => res.json());
  }
}

export { Chat };
