import React, { FormEvent, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../hooks/redux';
import { userActionCreator } from '../../store/actions';
import Error from '../error/Error';
import Preloader from '../preloader/Preloader';
import './Login.css';

const Login = () => {
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');
  const navigate = useNavigate();

  const { currentUser, preloader, errorMessage } = useAppSelector(state => ({
    currentUser: state.userReducer.currentUser,
    preloader: state.userReducer.preloader,
    errorMessage: state.userReducer.errorMessage
  }));
  const dispatch = useAppDispatch();

  const handleLogin = (event: FormEvent) => {
    event.preventDefault();
    if (login.length > 2 && password.length > 2) {
      dispatch(userActionCreator.login({ email: login, password }));
    }
  };

  if (currentUser?.id && !errorMessage) {
    if (currentUser.role === 'Admin') navigate('/users', { replace: true });
    else navigate('/chat', { replace: true });
  }

  if (preloader && !errorMessage) {
    return <Preloader />;
  }

  return (
    <div className="login-window">
      <form onSubmit={handleLogin} className="login-form">
        <input
          type="text"
          className="message-input-login"
          placeholder="Enter email"
          value={login}
          autoComplete="username"
          onChange={e => setLogin(e.target.value)}
        />
        <input
          type="password"
          className="message-input-password"
          placeholder="Enter password"
          value={password}
          autoComplete="current-password"
          onChange={e => setPassword(e.target.value)}
        />
        {errorMessage && <Error error={errorMessage} />}
        <button className="login-input-button" type="submit">
          Login
        </button>
      </form>
    </div>
  );
};

export default Login;
