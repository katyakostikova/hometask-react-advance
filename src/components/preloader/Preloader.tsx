/* eslint-disable react/self-closing-comp */
import React from 'react';
import './Preloader.css';

const Preloader = () => (
  <div className="preloader">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
  </div>
);

export default Preloader;
