/* eslint-disable react/jsx-one-expression-per-line */
import dayjs from 'dayjs';
import React from 'react';
import './Header.css';

interface HeaderProps {
  headerData: {
    participantsCount: number;
    messagesCount: number;
    lastMessageDate: string;
  };
}

const Header = ({ headerData }: HeaderProps) => {
  const lastMessageDate = dayjs(headerData.lastMessageDate).format(
    'DD.MM.YYYY HH:mm'
  );

  return (
    <div className="header">
      <div className="header-info">
        <h3 className="header-title">Hometask Chat</h3>
        <h3 className="header-users-count">
          {headerData.participantsCount} participants
        </h3>
        <h3 className="header-messages-count">
          {headerData.messagesCount} messages
        </h3>
      </div>
      <h3 className="header-last-message-date">
        Last message at {lastMessageDate}
      </h3>
    </div>
  );
};

export default Header;
