import React from 'react';
import { Link } from 'react-router-dom';
import { ERRORS } from '../../constants/http/errors';
import './Error.css';

interface ErrorPops {
  error: string;
}

const Error = ({ error }: ErrorPops) => (
  <div className="error-page">
    <h3 className="error-message">{error}</h3>
    {error !== ERRORS.LOGIN_FAILED ? (
      <Link className="error-back-button" to="/login">
        Back to login
      </Link>
    ) : (
      <></>
    )}
  </div>
);

export default Error;
