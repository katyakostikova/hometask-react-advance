import React from 'react';
import { Link } from 'react-router-dom';
import { UserModel } from '../../../models/user/User';
import './User.css';

interface UserProps {
  user: UserModel;
  onDelete: (id: string) => void;
}

const User = ({ user, onDelete }: UserProps) => {
  const handleUserDelete = () => onDelete(user.id);

  return (
    <li className="user-container">
      <div className="user-content">
        <div className="user-info">
          <p>{user.name}</p>
          <p>{user.email}</p>
        </div>
        <Link className="user-action-button" to={`/users/edit/${user.id}`}>
          Edit
        </Link>
        <button
          type="button"
          className="user-action-button"
          onClick={handleUserDelete}
        >
          Delete
        </button>
      </div>
    </li>
  );
};

export default User;
