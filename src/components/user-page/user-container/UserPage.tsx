import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../../hooks/redux';
import { userActionCreator } from '../../../store/actions';
import NotFound from '../../not-found/NotFound';
import Preloader from '../../preloader/Preloader';
import User from '../user/User';
import './UserPage.css';

const UserPage = () => {
  const dispatch = useAppDispatch();
  const { currentUser, preloader, users } = useAppSelector(state => ({
    currentUser: state.userReducer.currentUser,
    preloader: state.userReducer.preloader,
    users: state.userReducer.users
  }));

  const handleUserDelete = (id: string) => {
    const token = currentUser?.token ? currentUser.token : '';

    dispatch(userActionCreator.deleteUser({ userId: id, token }));
  };

  if (currentUser?.role !== 'Admin') return <NotFound />;

  useEffect(() => {
    dispatch(
      userActionCreator.fetchUsers(currentUser?.token ? currentUser.token : '')
    );
  }, []);

  if (preloader) return <Preloader />;

  return (
    <div className="user-page">
      <div className="user-page-button-container">
        <Link className="user-page-button" to="/users/edit">
          Add user
        </Link>

        <Link className="user-page-button" to="/chat">
          Open chat
        </Link>
      </div>

      <ol className="user-list">
        {users.map(user => (
          <User user={user} key={user.id} onDelete={handleUserDelete} />
        ))}
      </ol>
    </div>
  );
};

export default UserPage;
