import React, { ChangeEvent, FormEvent, useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../../hooks/redux';
import { userService } from '../../../services/services';
import { userActionCreator } from '../../../store/actions';
import NotFound from '../../not-found/NotFound';
import './UserEdit.css';

const initialUserValue = {
  id: '',
  name: '',
  email: '',
  password: '',
  avatar: ''
};

const UserEdit = () => {
  const [userData, setUserData] = useState(initialUserValue);
  const [isEdit, setIsEdit] = useState(false);

  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const currentUser = useAppSelector(state => state.userReducer.currentUser);
  const { id } = useParams();
  const token = currentUser?.token ? currentUser.token : '';

  if (currentUser?.role !== 'Admin') return <NotFound />;

  const handleInputChange = ({ target }: ChangeEvent<HTMLInputElement>) => {
    setUserData(state => ({ ...state, [target.name]: target.value }));
  };

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault();
    if (isEdit) dispatch(userActionCreator.updateUser({ userData, token }));
    else dispatch(userActionCreator.createUser({ userData, token }));
    navigate('/users', { replace: true });
  };

  useEffect(() => {
    const fetchUser = async () => {
      if (id) {
        const user = await userService.getUserById(id, token);
        user.password = '';
        setUserData({
          id: user.id,
          name: user.name,
          email: user.email,
          password: user.password,
          avatar: user.avatar
        });
        setIsEdit(true);
      }
    };

    fetchUser();
  }, [setUserData, userService]);

  return (
    <div className="edit-user-window">
      <form onSubmit={handleSubmit} className="edit-user-form">
        {isEdit ? <h3 className="edit-user-header">Edit User</h3> : <h3 className="edit-user-header">Add User</h3>}
        <input
          type="text"
          name="name"
          className="edit-input"
          placeholder="Enter username"
          minLength={3}
          required
          value={userData.name}
          onChange={e => handleInputChange(e)}
        />
        <input
          type="email"
          name="email"
          className="edit-input"
          placeholder="Enter email"
          required
          value={userData.email}
          onChange={e => handleInputChange(e)}
        />
        <input
          name="password"
          type="password"
          className="edit-input"
          placeholder="Enter password"
          minLength={3}
          required
          value={userData.password}
          onChange={e => handleInputChange(e)}
        />
        <input
          type="url"
          name="avatar"
          className="edit-input"
          placeholder="Enter avatar url"
          value={userData.avatar}
          onChange={e => handleInputChange(e)}
        />
        <button className="edit-input-button" type="submit">
          {isEdit ? 'Edit user' : 'Add user'}
        </button>
      </form>
    </div>
  );
};

export default UserEdit;
