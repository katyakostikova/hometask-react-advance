import React, { useCallback, useEffect } from 'react';
import Header from '../../header/Header';
import Preloader from '../../preloader/Preloader';
import './Chat.css';
import MessageList from '../message_list/MessageList';

import { useAppDispatch, useAppSelector } from '../../../hooks/redux';
import { chatActionCreator } from '../../../store/actions';
import MessageInput from '../message_input/MessageInput';
import EditMessageModal from '../edit-modal/EditMessageModal';
import { keyCodes } from '../../../constants/chat/keyCodes';
import {
  checkLastUserMessage,
  getChatParticipants
} from '../../../helpers/chat/chat';
import Error from '../../error/Error';

const Chat = () => {
  const dispatch = useAppDispatch();
  const { chat, preloader, currentUser, editModal, errorMessage } = useAppSelector(state => ({
    chat: state.chatReducer.chat,
    preloader: state.chatReducer.preloader,
    currentUser: state.userReducer.currentUser,
    editModal: state.chatReducer.editModal,
    errorMessage: state.chatReducer.errorMessage
  }));
  const token = currentUser?.token ? currentUser.token : '';
  const currentUserId = currentUser?.id ? currentUser.id : '0';

  const handleUserClick = useCallback(
    (event: KeyboardEvent) => {
      if (event.code === keyCodes.KEYUP) {
        const messageId = checkLastUserMessage(chat.messages, currentUserId);
        if (messageId) {
          dispatch(
            chatActionCreator.openMessageEditModal({
              messageId: messageId.toString(),
              token
            })
          );
        }
      }
    },
    [chat.messages]
  );

  useEffect(() => {
    if (chat.messages.length === 0) dispatch(chatActionCreator.loadMessages(token));

    window.addEventListener('keydown', handleUserClick);

    return () => {
      window.removeEventListener('keydown', handleUserClick);
    };
  }, [handleUserClick]);

  if (errorMessage) return <Error error={errorMessage} />;

  if (preloader) {
    return <Preloader />;
  }

  const headerData = {
    participantsCount: getChatParticipants(chat.messages),
    messagesCount: chat.messages.length,
    lastMessageDate: chat.messages[chat.messages.length - 1].createdAt
  };
  return (
    <div className="chat">
      <Header headerData={headerData} />
      {editModal && <EditMessageModal token={token} />}
      <MessageList allMessages={chat.messages} token={token} />
      <MessageInput token={token} />
    </div>
  );
};

export default Chat;
