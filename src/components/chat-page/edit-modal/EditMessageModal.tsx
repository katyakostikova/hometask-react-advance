import React, { useState } from 'react';
import { useAppDispatch, useAppSelector } from '../../../hooks/redux';
import { chatActionCreator } from '../../../store/actions';
import './EditMessageModal.css';

interface EditMessageModalProps {
  token: string;
}

const EditMessageModal = ({ token }: EditMessageModalProps) => {
  const editMessage = useAppSelector(state => state.chatReducer.editMessage);
  const [editedMessage, setEditedMessage] = useState(editMessage?.text);
  const dispatch = useAppDispatch();

  const handleCloseModal = () => {
    dispatch(chatActionCreator.closeModal());
  };

  const handleEditMessage = () => {
    if (!editedMessage || editedMessage.trim().length < 1) return;
    const messageId = editMessage?.id ? editMessage.id : '-1';
    const liked = editMessage?.liked ? editMessage.liked : false;
    dispatch(
      chatActionCreator.editMessage({
        messageId,
        text: editedMessage,
        liked,
        token
      })
    );
  };

  return (
    <div className="edit-message-modal">
      <div className="modal-shown">
        <div className="modal-header">
          <h2>Edit message</h2>
          <hr />
        </div>
        <div className="modal-content">
          <textarea
            className="edit-message-input"
            value={editedMessage}
            onChange={e => setEditedMessage(e.target.value)}
          />
        </div>
        <div className="modal-button-container">
          <button
            className="edit-message-button"
            type="button"
            onClick={handleEditMessage}
          >
            Ok
          </button>
          <button
            className="edit-message-close"
            type="button"
            onClick={handleCloseModal}
          >
            Cancel
          </button>
        </div>
      </div>
    </div>
  );
};

export default EditMessageModal;
