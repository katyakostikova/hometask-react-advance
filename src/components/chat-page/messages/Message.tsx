import React from 'react';
import dayjs from 'dayjs';
import './Message.css';
import LikeFilledImage from '../../../assets/images/like-filled.svg';
import LikeImage from '../../../assets/images/like-outlined.svg';
import { MessageModel } from '../../../models/chat/Message';
import { USER_DEFAULT_IMAGE } from '../../../constants/chat/image-user-default';
import { useAppDispatch } from '../../../hooks/redux';
import { chatActionCreator } from '../../../store/actions';

interface MessageProps {
  messageData: MessageModel;
  token: string;
}

const Message = ({ messageData, token }: MessageProps) => {
  const dispatch = useAppDispatch();

  const likeButtonHandler = () => {
    dispatch(
      chatActionCreator.editMessage({
        messageId: messageData.id,
        text: messageData.text,
        liked: !messageData.liked,
        token
      })
    );
  };

  const messageDate = messageData.updatedAt
    ? `Edited at: ${dayjs(messageData.updatedAt).format('DD:MM:YY HH:mm')}`
    : dayjs(messageData.createdAt).format('HH:mm');

  return (
    <li className={messageData.liked ? 'message message-liked' : 'message'}>
      <div className="message-user-avatar">
        <img
          src={messageData.avatar ? messageData.avatar : USER_DEFAULT_IMAGE}
          alt="User avatar"
        />
      </div>
      <div className="message-content">
        <h3 className="message-user-name">{messageData.user}</h3>
        <p className="message-text">{messageData.text}</p>
        <p className="message-time">{messageDate}</p>
      </div>
      <button
        type="button"
        className="message-like"
        onClick={likeButtonHandler}
      >
        <img
          src={messageData.liked ? LikeFilledImage : LikeImage}
          alt="Like Button"
        />
      </button>
    </li>
  );
};

export default Message;
