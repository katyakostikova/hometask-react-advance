import React from 'react';
import dayjs from 'dayjs';
import './OwnMessage.css';
import EditImage from '../../../assets/images/edit.svg';
import DeleteImage from '../../../assets/images/delete.svg';
import { MessageModel } from '../../../models/chat/Message';
import { useAppDispatch } from '../../../hooks/redux';
import { chatActionCreator } from '../../../store/actions';

interface OwnMessageProps {
  messageData: MessageModel;
  token: string;
}

const OwnMessage = ({ messageData, token }: OwnMessageProps) => {
  const dispatch = useAppDispatch();

  const messageDate = messageData.updatedAt
    ? `Edited at: ${dayjs(messageData.updatedAt).format('DD:MM:YY HH:mm')}`
    : dayjs(messageData.createdAt).format('HH:mm');

  const deleteMessageHandler = () => {
    dispatch(
      chatActionCreator.deleteMessage({ messageId: messageData.id, token })
    );
  };

  const showModalEditMessage = () => {
    dispatch(
      chatActionCreator.openMessageEditModal({
        messageId: messageData.id,
        token
      })
    );
  };

  return (
    <li className="own-message">
      <div className="own-message-content">
        <p className="message-text">{messageData.text}</p>
        <p className="message-time">{messageDate}</p>
      </div>
      <div className="message-buttons-container">
        <button
          type="button"
          className="message-edit"
          onClick={showModalEditMessage}
        >
          <img src={EditImage} alt="Edit Message Button" />
        </button>
        <button
          type="button"
          className="message-delete"
          onClick={deleteMessageHandler}
        >
          <img src={DeleteImage} alt="Delete Message Button" />
        </button>
      </div>
    </li>
  );
};

export default OwnMessage;
