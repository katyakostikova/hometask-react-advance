/* eslint-disable @typescript-eslint/no-non-null-assertion */
import React, { useEffect, useRef } from 'react';
import Message from '../messages/Message';
import './MessageList.css';
import OwnMessage from '../messages/OwnMessage';
import { OLD_DATE } from '../../../constants/enums/date';
import Divider from './divider/Divider';
import { MessageModel } from '../../../models/chat/Message';
import { useAppSelector } from '../../../hooks/redux';
import {
  checkDividerDate,
  checkIsDifferentDate
} from '../../../helpers/divider/divider';

interface MessageListProps {
  allMessages: MessageModel[];
  token: string;
}

const MessageList = ({ allMessages, token }: MessageListProps) => {
  const messagesEndRef = useRef<HTMLOListElement | null>(null);

  const currentUser = useAppSelector(state => state.userReducer.currentUser);
  const currentUserId = currentUser?.id ? currentUser.id : '0';

  // scroll messages to the last
  useEffect(() => {
    const lastChild = messagesEndRef.current!.lastElementChild;
    lastChild?.scrollIntoView({ behavior: 'smooth' });
  }, [allMessages]);

  let prevMessageDate = OLD_DATE;

  return (
    <ol ref={messagesEndRef} className="message-list">
      {allMessages.map(message => {
        const isDivided = checkIsDifferentDate(
          prevMessageDate,
          message.createdAt
        );
        const dividerDate = isDivided
          ? checkDividerDate(message.createdAt)
          : '';

        prevMessageDate = message.createdAt;

        return (
          <div
            className={
              message.userId === +currentUserId
                ? 'own-message-container'
                : 'message-container'
            }
            key={message.id}
          >
            {isDivided && <Divider dayText={dividerDate} />}
            {message.userId === +currentUserId ? (
              <OwnMessage messageData={message} token={token} />
            ) : (
              <Message messageData={message} token={token} />
            )}
          </div>
        );
      })}
    </ol>
  );
};

export default MessageList;
