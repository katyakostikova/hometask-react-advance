import React from 'react';
import './Divider.css';

interface DividerProps {
  dayText: string;
}

const Divider = ({ dayText }: DividerProps) => (
  <div className="messages-divider">
    <hr />
    <p className="divider-text">{dayText}</p>
  </div>
);

export default Divider;
