import React, { useState } from 'react';
import { useAppDispatch } from '../../../hooks/redux';
import { chatActionCreator } from '../../../store/actions';
import './MessageInput.css';

interface MessageInputProps {
  token: string;
}

const MessageInput = ({ token }: MessageInputProps) => {
  const [inputtedMessage, setInputtedMessage] = useState('');
  const dispatch = useAppDispatch();

  const submitHandler = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (!inputtedMessage || inputtedMessage.trim().length < 1) return;
    dispatch(
      chatActionCreator.addMessage({
        message: { text: inputtedMessage, liked: false },
        token
      })
    );
  };

  return (
    <div className="message-input">
      <form onSubmit={e => submitHandler(e)}>
        <input
          type="text"
          className="message-input-text"
          value={inputtedMessage}
          minLength={1}
          onChange={e => setInputtedMessage(e.target.value)}
          placeholder="Enter new Message"
        />
        <button className="message-input-button" type="submit">
          Send
        </button>
      </form>
    </div>
  );
};

export default MessageInput;
