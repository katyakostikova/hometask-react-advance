import React from 'react';
import { Link } from 'react-router-dom';
import './NotFound.css';
import NotFoundImage from '../../assets/images/404.png';

const NotFound = () => (
  <div className="page-not-found">
    <img
      className="page-not-found-image"
      alt="Page not found"
      src={NotFoundImage}
    />
    <h1 className="page-not-found-header">Page not found</h1>
    <Link className="page-not-found-button" to="/login">Back to login</Link>
  </div>
);

export default NotFound;
