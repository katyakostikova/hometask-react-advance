/* eslint-disable @typescript-eslint/no-empty-function */
import { createReducer, isAnyOf } from '@reduxjs/toolkit';
import { UserModel } from '../../models/user/User';
import * as userActions from './actions';

interface UserState {
  currentUser: UserModel | undefined;
  preloader: boolean;
  users: UserModel[];
  errorMessage: string | undefined;
}

const initialState: UserState = {
  currentUser: undefined,
  preloader: false,
  users: [],
  errorMessage: undefined
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(userActions.login.fulfilled, (state, action) => {
    const currentUser = action.payload;
    state.preloader = false;
    state.currentUser = currentUser;
  });
  builder.addCase(userActions.fetchUsers.fulfilled, (state, action) => {
    const users = action.payload;
    state.preloader = false;
    state.users = users;
  });
  builder.addCase(userActions.deleteUser.fulfilled, (state, action) => {
    const deletedUserId = action.payload;
    state.preloader = false;
    const userToDeleteIndex = state.users.findIndex(
      user => user.id === deletedUserId
    );
    state.users.splice(userToDeleteIndex, 1);
  });
  builder.addCase(userActions.createUser.fulfilled, (state, action) => {
    const newUser: UserModel = action.payload;
    state.preloader = false;
    state.users.push(newUser);
  });
  builder.addCase(userActions.updateUser.fulfilled, (state, action) => {
    const updatedUser = action.payload;
    state.preloader = false;
    const userToUpdateIndex = state.users.findIndex(
      user => user.id === updatedUser.id
    );
    state.users.splice(userToUpdateIndex, 1, updatedUser);
  });
  builder.addCase(
    userActions.login.rejected,

    (state, action) => {
      const errorMessage = action.payload;
      state.errorMessage = errorMessage;
    }
  );
  builder.addMatcher(
    isAnyOf(
      userActions.login.pending,
      userActions.deleteUser.pending,
      userActions.createUser.pending,
      userActions.updateUser.pending
    ),
    state => {
      state.preloader = true;
      state.errorMessage = undefined;
    }
  );
});

export { reducer };
