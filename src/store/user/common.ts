const ActionType = {
  LOGIN: 'login',
  FETCH_USERS: 'fetch-users',
  DELETE_USER: 'delete-user',
  CREATE_USER: 'create-user',
  UPDATE_USER: 'update-user'
};

export { ActionType };
