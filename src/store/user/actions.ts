import { createAsyncThunk } from '@reduxjs/toolkit';
import { LoginModel } from '../../models/user/Login';
import { UserModel } from '../../models/user/User';
import { User } from '../../services/user/user-service';
import { AppDispatch, RootState } from '../store';
import { ActionType } from './common';
import { UserEditModel } from '../../models/user/UserEdit';
import { ERRORS } from '../../constants/http/errors';

interface ThunkApiModel {
  dispatch: AppDispatch;
  state: RootState;
  rejectValue: string;
  extra: {
    services: {
      user: User;
    };
  };
}

interface DeleteUserModel {
  userId: string;
  token: string;
}

interface CreateUserModel {
  userData: UserEditModel;
  token: string;
}

const login = createAsyncThunk<UserModel, LoginModel, ThunkApiModel>(
  ActionType.LOGIN,
  async (
    userData,
    {
      rejectWithValue,
      extra: {
        services: { user }
      }
    }
  ) => {
    try {
      const currentUser: UserModel = await user.login(userData);

      return currentUser;
    } catch (error) {
      return rejectWithValue(ERRORS.LOGIN_FAILED);
    }
  }
);

const fetchUsers = createAsyncThunk<UserModel[], string, ThunkApiModel>(
  ActionType.FETCH_USERS,
  async (
    token,
    {
      extra: {
        services: { user }
      }
    }
  ) => {
    const users: UserModel[] = await user.loadUsers(token);

    return users;
  }
);

const deleteUser = createAsyncThunk<string, DeleteUserModel, ThunkApiModel>(
  ActionType.DELETE_USER,
  async (
    { userId, token },
    {
      extra: {
        services: { user }
      }
    }
  ) => {
    await user.deleteUserById(userId, token);

    return userId;
  }
);

const createUser = createAsyncThunk<UserModel, CreateUserModel, ThunkApiModel>(
  ActionType.CREATE_USER,
  async (
    { userData, token },
    {
      extra: {
        services: { user }
      }
    }
  ) => {
    delete userData.id;
    const newUser = await user.createUser(userData, token);

    return newUser;
  }
);

const updateUser = createAsyncThunk<UserModel, CreateUserModel, ThunkApiModel>(
  ActionType.UPDATE_USER,
  async (
    { userData, token },
    {
      extra: {
        services: { user }
      }
    }
  ) => {
    const id = userData?.id ? userData.id : '0';

    delete userData.id;
    await user.updateUser(id, userData, token);

    const updatedUser = await user.getUserById(id, token);
    return updatedUser;
  }
);

export { login, fetchUsers, deleteUser, createUser, updateUser };
