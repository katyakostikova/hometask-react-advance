/* eslint-disable no-shadow */
import { combineReducers, configureStore } from '@reduxjs/toolkit';
import { chatService, userService } from '../services/services';
import { reducer as chatReducer } from './chat/reducer';
import { reducer as userReducer } from './user/reducer';

export const rootReducer = combineReducers({
  chatReducer,
  userReducer
});

const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware => getDefaultMiddleware({
    thunk: {
      extraArgument: {
        services: {
          chat: chatService,
          user: userService
        }
      }
    }
  })
});

export type RootState = ReturnType<typeof rootReducer>;
export type AppDispatch = typeof store.dispatch;
export type AppStore = typeof store;

export default store;
