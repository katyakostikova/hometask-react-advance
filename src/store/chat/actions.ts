import { createAction, createAsyncThunk } from '@reduxjs/toolkit';
import { ERRORS } from '../../constants/http/errors';
import { MessageModel } from '../../models/chat/Message';
import { NewMessageModel } from '../../models/chat/NewMessage';
import { Chat } from '../../services/chat/chat-service';
import { AppDispatch, RootState } from '../store';
import { ActionType } from './common';

interface ThunkApiModel {
  dispatch: AppDispatch;
  state: RootState;
  rejectValue: string;
  extra: {
    services: {
      chat: Chat;
    };
  };
}

interface AddMessageModel {
  message: NewMessageModel;
  token: string;
}

interface DeleteOrModalMessageModel {
  messageId: string;
  token: string;
}

interface EditMessageModel {
  messageId: string;
  text: string;
  liked: boolean;
  token: string;
}

const loadMessages = createAsyncThunk<MessageModel[], string, ThunkApiModel>(
  ActionType.LOAD_MESSAGES,
  async (
    jwt,
    {
      rejectWithValue,
      extra: {
        services: { chat }
      }
    }
  ) => {
    try {
      const messages: MessageModel[] = await chat.getAllMessages(jwt);

      return messages;
    } catch (error) {
      return rejectWithValue(ERRORS.UNAUTHORIZED);
    }
  }
);

const addMessage = createAsyncThunk<
  MessageModel,
  AddMessageModel,
  ThunkApiModel
>(
  ActionType.ADD_MESSAGE,
  async (
    { message, token },
    {
      extra: {
        services: { chat }
      }
    }
  ) => {
    const newMessage: MessageModel = await chat.addMessage(message, token);

    return newMessage;
  }
);

const deleteMessage = createAsyncThunk<
  string,
  DeleteOrModalMessageModel,
  ThunkApiModel
>(
  ActionType.DELETE_MESSAGE,
  async (
    { messageId, token },
    {
      extra: {
        services: { chat }
      }
    }
  ) => {
    await chat.deleteMessageById(messageId, token);

    return messageId;
  }
);

const openMessageEditModal = createAsyncThunk<
  MessageModel,
  DeleteOrModalMessageModel,
  ThunkApiModel
>(
  ActionType.OPEN_MODAL,
  async (
    { messageId, token },
    {
      extra: {
        services: { chat }
      }
    }
  ) => {
    const message = await chat.getMessageById(messageId, token);

    return message;
  }
);

const closeModal = createAction(ActionType.CLOSE_MODAL);

const editMessage = createAsyncThunk<
  MessageModel,
  EditMessageModel,
  ThunkApiModel
>(
  ActionType.EDIT_MESSAGE,
  async (
    { messageId, text, liked, token },
    {
      extra: {
        services: { chat }
      }
    }
  ) => {
    await chat.editMessage(messageId, { text, liked }, token);
    const message = await chat.getMessageById(messageId, token);
    return message;
  }
);

export {
  loadMessages,
  addMessage,
  deleteMessage,
  openMessageEditModal,
  closeModal,
  editMessage
};
