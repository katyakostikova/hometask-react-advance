import { createReducer, isAnyOf } from '@reduxjs/toolkit';
import { MessageModel } from '../../models/chat/Message';
import * as chatActions from './actions';

interface ChatState {
  chat: {
    messages: MessageModel[];
  };
  editModal: boolean;
  editMessage: MessageModel | undefined;
  preloader: boolean;
  errorMessage: string | undefined;
}

const initialState: ChatState = {
  chat: {
    messages: []
  },
  editModal: false,
  editMessage: undefined,
  preloader: true,
  errorMessage: undefined
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(chatActions.loadMessages.fulfilled, (state, action) => {
    const messages: MessageModel[] = action.payload;

    state.chat.messages = messages.sort(
      (a, b) => new Date(a.createdAt).valueOf() - new Date(b.createdAt).valueOf()
    );
    state.preloader = false;
  });
  builder.addCase(chatActions.addMessage.fulfilled, (state, action) => {
    const newMessage: MessageModel = action.payload;

    state.chat.messages.push(newMessage);
    state.preloader = false;
  });
  builder.addCase(chatActions.deleteMessage.fulfilled, (state, action) => {
    const deletedMessageId: string = action.payload;

    const messageToDeleteIndex = state.chat.messages.findIndex(
      message => message.id === deletedMessageId
    );
    state.chat.messages.splice(messageToDeleteIndex, 1);
    state.preloader = false;
  });
  builder.addCase(
    chatActions.openMessageEditModal.fulfilled,
    (state, action) => {
      const message: MessageModel = action.payload;

      state.editMessage = message;
      state.editModal = true;
      state.preloader = false;
    }
  );
  builder.addCase(chatActions.closeModal, state => {
    state.editModal = false;
    state.editMessage = undefined;
  });
  builder.addCase(chatActions.editMessage.fulfilled, (state, action) => {
    const editedMessage: MessageModel = action.payload;

    const messageToUpdateIndex = state.chat.messages.findIndex(
      message => message.id === editedMessage.id
    );
    state.chat.messages.splice(messageToUpdateIndex, 1, editedMessage);
    state.preloader = false;
    state.editModal = false;
    state.editMessage = undefined;
  });
  builder.addMatcher(
    isAnyOf(
      chatActions.addMessage.pending,
      chatActions.deleteMessage.pending,
      chatActions.openMessageEditModal.pending,
      chatActions.editMessage.pending
    ),
    state => {
      state.preloader = true;
      state.errorMessage = undefined;
    }
  );
  builder.addMatcher(
    isAnyOf(chatActions.loadMessages.rejected),
    (state, action) => {
      const errorMessage = action.payload;
      state.errorMessage = errorMessage;
    }
  );
});

export { reducer };
