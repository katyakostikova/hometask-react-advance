const ActionType = {
  LOAD_MESSAGES: 'load-messages',
  ADD_MESSAGE: 'add-message',
  DELETE_MESSAGE: 'delete-message',
  OPEN_MODAL: 'open-modal',
  CLOSE_MODAL: 'close-modal',
  EDIT_MESSAGE: 'edit-message'
};

export { ActionType };
