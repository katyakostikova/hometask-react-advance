export const ERRORS = {
  LOGIN_FAILED: 'Email or password is wrong',
  UNAUTHORIZED: 'Please login to enter the chat. Please go back and reload the page'
};
