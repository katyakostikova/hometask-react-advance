import Chat from './src/components/chat-page/chat/Chat';
import { rootReducer } from './src/store/store';

export default {
  Chat,
  rootReducer
};
